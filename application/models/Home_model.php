<?php


class Home_model extends CI_Model
{
    public function addPlainte($data)
    {
        $this->db->insert('plainte', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getLastNum()
    {
        $this->db->select("numero_dossier");
        $this->db->from("plainte");
        $this->db->limit(1);
        $this->db->order_by('id_plainte',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function piecesJoints($filename, $token)
	{
		$data = array(
			'token' => $token,
			'files' => $filename
		);
		$this->db->insert('piecesjointe_plainte', $data);
	}


    public function getEmailSecretariat($nom_type)
    {
        $this->db->select("email");
        $this->db->from("utilisateurs");
        $this->db->join("type_compte as tc", "tc.id_type = utilisateurs.id_type");
        $this->db->where("tc.nom_type", $nom_type);
        $this->db->where("utilisateurs.date_delete is null");
        $this->db->where("tc.date_delete_type is null");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
