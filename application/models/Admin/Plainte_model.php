<?php
class Plainte_model extends CI_Model
{
    public function getDossierPlainte($numeroDossier)
    {
        $this->db->select('id_plainte, numero_dossier, nom_prenom, date_naissance, nationalite, telephon, email, adresse, profession, sexe, statut_personne, nom_plainte, tel_plainte, adresse_plainte, nature_plainte, langue_plainte, description_plainte, date_ajout_plainte');
        $this->db->from('plainte');
        $this->db->where('id_plainte', $numeroDossier);
        $query = $this->db->get();
        return $query->result();
    }
    public function validerPlainte($id_plainte, $data)
    {
        $this->db->where('id_plainte', $id_plainte);
        $this->db->update('plainte', $data);
        return true;
    }
    public function getNumeroPlainte($decision)
    {
        $this->db->select('numero_dossier, id_plainte');
        $this->db->from('plainte');
        $this->db->where('decision_sg', $decision);
        $this->db->where('agent_affecter is null');
        $this->db->where('secretariat_recu is not null');
        $query = $this->db->get();
        return $query->result();
    }
    public function getNumeroPlainteAffecte()
    {
        $this->db->select('numero_dossier, id_plainte');
        $this->db->from('plainte');
        $this->db->where('agent_affecter is not null');
        $this->db->where('secretariat_recu is not null');
        $this->db->where('affect_sg is not null');
        $this->db->where('plainte_affect is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function AffecterPlainte($id_plainte, $data)
    {
        $this->db->where('id_plainte', $id_plainte);
        $this->db->update('plainte', $data);
        return true;
    }
    public function getPlainteAffecter($id)
    {
        $this->db->select('id_plainte, numero_dossier, nom_prenom, date_naissance, nationalite, telephon, email, adresse');
        $this->db->from('plainte');
        $this->db->where('agent_affecter', $id);
        $this->db->where('affect_sg is not null');
        $this->db->where('plainte_affect is null');
        $this->db->order_by('id_plainte asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function AffecterPlainteSG($data)
    {
        $this->db->insert('affect_plainte', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function updatePlainteAffect($id, $data){
        $this->db->where('id_plainte', $id);
        $this->db->update('plainte', $data);
        return true;
    }
    public function updatePlainte($id, $data){
        $this->db->where('id_plainte', $id);
        $this->db->update('plainte', $data);
        return true;
    }
    public function getAllPlainteSG()
    {
        $this->db->select('plainte.id_plainte, numero_dossier, nom_prenom, date_naissance, nationalite, telephon, email, adresse, profession, sexe, statut_personne, nom_plainte, tel_plainte, adresse_plainte, nature_plainte, langue_plainte, description_plainte, date_ajout_plainte');
        $this->db->from('plainte');
        $this->db->where('affect_sg IS NOT NULL');
        $this->db->where('decision_sg IS NULL');
        $query = $this->db->get();
        return $query->result();
    }
    public function plainteAccepterSG($id, $data)
    {
        $this->db->where('id_plainte', $id);
        $this->db->update('plainte', $data);
        return true;
    }
    public function getAllPlainteDecisionSG()
    {
        $this->db->select('plainte.id_plainte, numero_dossier, nom_prenom, date_naissance, nationalite, telephon, email, adresse, profession, sexe, statut_personne, nom_plainte, tel_plainte, adresse_plainte, nature_plainte, langue_plainte, description_plainte, date_ajout_plainte');
        $this->db->from('plainte');
        $this->db->join('decision_plainte as dp', 'plainte.id_plainte = dp.id_plainte');
        $this->db->where('affect_sg IS NOT NULL');
        $this->db->where('dp.date_decision IS NULL');
        $query = $this->db->get();
        return $query->result();
    }
    public function plainteAccepter($id, $data)
    {
        $this->db->where('id_plainte', $id);
        $this->db->update('affect_plainte', $data);
        return true;
    }
    public function getAllPlainteSG_Valid()
    {
        $this->db->select('p.numero_dossier, ap.id_plainte, ap.commentaire');
        $this->db->from('affect_plainte as ap');
        $this->db->join('plainte as p', 'ap.id_plainte = p.id_plainte');
        $this->db->join('decision_plainte as dp', 'ap.id_plainte = dp.id_plainte');
        $this->db->where('p.type_plainte is null');
        $this->db->where('dp.type_plainte is NULL');
        $query = $this->db->get();
        return $query->result();
    }
    public function decisionPlainteSG($data)
    {
        $this->db->insert('decision_plainte', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function TypePlainte($id, $data){
        $this->db->where('id_decision', $id);
        $this->db->update('decision_plainte', $data);
        return true;
    }
    public function TypePlainte2($id, $data){
        $this->db->where('id_plainte', $id);
        $this->db->update('plainte', $data);
        return true;
    }
    public function getEmailAgent($agent)
    {
        $this->db->select('email');
        $this->db->from('utilisateurs');
        $this->db->where('id_user', $agent);
        $this->db->where('date_delete is NULL');
        $query = $this->db->get();
        return $query->result();
    }

    public function getEmailSG($nom_type)
    {
        $this->db->select("email");
        $this->db->from("utilisateurs");
        $this->db->join("type_compte as tc", "tc.id_type = utilisateurs.id_type");
        $this->db->where("tc.nom_type", $nom_type);
        $this->db->where("utilisateurs.date_delete is null");
        $this->db->where("tc.date_delete_type is null");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function getEmailPresident($nom_type)
    {
        $this->db->select("email");
        $this->db->from("utilisateurs");
        $this->db->join("type_compte as tc", "tc.id_type = utilisateurs.id_type");
        $this->db->where("tc.nom_type", $nom_type);
        $this->db->where("utilisateurs.date_delete is null");
        $this->db->where("tc.date_delete_type is null");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    public function getAllConvention(){
        $this->db->select("id_type_plainte, nom_type_plainte");
        $this->db->from("type_plainte");
        $this->db->where("date_delete is null");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

}