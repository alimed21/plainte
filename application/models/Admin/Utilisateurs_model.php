<?php


class Utilisateurs_model extends CI_Model
{
    public function getAllUser()
    {
        $this->db->select('id_user, username, email, t.nom_type,ad.login_admin, date_add');
        $this->db->from('utilisateurs');
        $this->db->join('type_compte as t', 'utilisateurs.id_type = t.id_type');
        $this->db->join('administrateur as ad', 'utilisateurs.id_admin_add = ad.id_admin');
        $this->db->where('utilisateurs.date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function addUser($data)
    {
        $this->db->insert('utilisateurs', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function suppressionUtilisateur($id, $data)
    {
        $this->db->where('id_user', $id );
        $this->db->update('utilisateurs', $data);
        return true;
    }
}