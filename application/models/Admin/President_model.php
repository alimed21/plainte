<?php


class President_model extends CI_Model
{
    public function countAllPlaint()
    {
        $this->db->select('COUNT(id_plainte) as plaintes');
        $this->db->from('plainte');
       // $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }
   /* public function countAllPlaintByYear(){
        $this->db->select('COUNT(id_plainte) as plaintes, DISTINCT YEAR(date_ajout_plainte)');
        $this->db->from('plainte');
        $this->db->group_by('date_ajout_plainte');

        $query = $this->db->get();
        return $query->result();
    }*/
    public function countPlaintesRecevable($recevable)
    {
        $this->db->select('COUNT(id_plainte) as recevable');
        $this->db->from('decision_plainte');
        $this->db->where('decision', $recevable);
        $query = $this->db->get();
        return $query->result();
    }
    public function countPlaintesSimple($simple)
    {
        $this->db->select('COUNT(id_plainte) as simple');
        $this->db->from('decision_plainte');
        $this->db->where('type_plainte', $simple);
        $query = $this->db->get();
        return $query->result();
    }
    public function countPlaintesComplexe($complexe)
    {
        $this->db->select('COUNT(id_plainte) as complexe');
        $this->db->from('decision_plainte');
        $this->db->where('type_plainte', $complexe);
        $query = $this->db->get();
        return $query->result();
    }
    public function countPlaintesParMois()
    {
        $this->db->select('COUNT(id_plainte) as count, DATE_FORMAT(date_ajout_plainte,"%m") as month');
        $this->db->from('plainte');
        //$this->db->where('date_delete is null');
        $this->db->group_by('DATE_FORMAT(date_ajout_plainte,"%m")');
        $query = $this->db->get();
        return $query->result();
    }
    public function typeUser(){
        $this->db->select('nom_type, COUNT(u.id_type) as nbr');
        $this->db->from('type_compte');
        $this->db->join('utilisateurs as u', 'type_compte.id_type = u.id_type');
        $this->db->where('date_delete_type is null');
        $this->db->where('date_delete is null');
        $this->db->group_by('nom_type');
        $query = $this->db->get();
        return $query->result();
    }
    public function ListUsers()
    {
        $this->db->select('nom_type, username');
        $this->db->from('type_compte');
        $this->db->join('utilisateurs as u', 'type_compte.id_type = u.id_type');
        $this->db->where('date_delete_type is null');
        $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function genreHomme($homme)
    {
        $this->db->select('COUNT(id_plainte) as sexe');
        $this->db->from('plainte');
        //$this->db->where('date_delete is null');
        $this->db->where('sexe', $homme);
        $query = $this->db->get();
        return $query->result();
    }
    public function genreFemme($femme)
    {
        $this->db->select('COUNT(id_plainte) as sexe');
        $this->db->from('plainte');
       // $this->db->where('date_delete is null');
        $this->db->where('sexe', $femme);
        $query = $this->db->get();
        return $query->result();
    }
    public function genreEnfant($enfant)
    {
        $this->db->select('COUNT(id_plainte) as sexe');
        $this->db->from('plainte');
       // $this->db->where('date_delete is null');
        $this->db->where('sexe', $enfant);
        $query = $this->db->get();
        return $query->row();
    }
    public function handicap($handicap)
    {
        $this->db->select('COUNT(id_plainte) as handicap');
        $this->db->from('plainte');
       // $this->db->where('date_delete is null');
        $this->db->where('handicap', $handicap);
        $query = $this->db->get();
        return $query->row();
    }
    public function Nonhandicap($nonhandicap)
    {
        $this->db->select('COUNT(id_plainte) as nonhandicap');
        $this->db->from('plainte');
       // $this->db->where('date_delete is null');
        $this->db->where('handicap', $nonhandicap);
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllPlainte($recevable)
    {
        $this->db->select('p.id_plainte, p.numero_dossier, dp.decision, dp.type_plainte');
        $this->db->from('plainte as p');
        $this->db->join('decision_plainte as dp', 'dp.id_plainte = p.id_plainte');
        $this->db->where('dp.decision', $recevable);
        $this->db->where('dp.decision_president is null');
        //$this->db->where('p.date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function countPlaintesRejeter($rejecter)
    {
        $this->db->select('COUNT(id_plainte) as irrecevable');
        $this->db->from('decision_plainte');
        $this->db->where('decision_president', $rejecter);
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllDate(){
        $sql = 'SELECT count(date_format(now(), "%Y") - date_format(date_naissance, "%Y")) as under_18 FROM plainte 
          where (date_format(now(), "%Y") - date_format(date_naissance, "%Y")) < 18';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function decisionPresident($id_plainte, $data)
    {
        $this->db->where('id_plainte', $id_plainte);
        $this->db->update('decision_plainte', $data);
        return true;
    }
    public function plainteParConvention(){
        $this->db->select('COUNT(id_plainte) as convention, nom_court');
        $this->db->from('decision_plainte');
        $this->db->join('type_plainte as con', 'con.id_type_plainte = decision_plainte.type_convention');
        $this->db->group_by("nom_court");
        $query = $this->db->get();
        return $query->result();
    }
}