<?php


class Home_model extends CI_Model
{
    public function addAnnuaire($data)
    {
        $this->db->insert('annuaire', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getInfoUser($id)
    {
        $this->db->select('username, email, p.image_user');
        $this->db->from('utilisateurs');
        $this->db->join('profile as p', 'utilisateurs.id_user = p.id_user');
        $this->db->where('utilisateurs.id_user', $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function getInfoAdmin($id_admin)
    {
        $this->db->select('nom_admin, image_admin');
        $this->db->from('profileadmin');
        $this->db->where('id_admin', $id_admin);
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPlainte()
    {
        $this->db->select('id_plainte, numero_dossier, nom_prenom, date_naissance, nationalite, telephon, email, adresse');
        $this->db->from('plainte');
        $this->db->where('agent_affecter IS NULL');
        $this->db->where('secretariat_recu IS NULL');
        $this->db->order_by('id_plainte asc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPieceJoint(){
        $this->db->select('id_fichier, token, files');
        $this->db->from('piecesjointe_plainte');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllAgent($typeUser)
    {
        $this->db->select('id_user, username');
        $this->db->from('utilisateurs');
        $this->db->join('type_compte as t', 'utilisateurs.id_type = t.id_type');
        $this->db->where('t.nom_type', $typeUser);
        $this->db->where('utilisateurs.date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }

    public function getCountPlainte(){
        $this->db->select('COUNT(id_plainte) as notification');
        $this->db->from('plainte');
        $this->db->where('agent_affecter IS NULL');
        $this->db->where('secretariat_recu IS NULL');
        $this->db->order_by('id_plainte asc');
        $query = $this->db->get();

        $ret = $query->row();
        return $ret->notification;
    }
}