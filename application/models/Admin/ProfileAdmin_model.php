<?php


class ProfileAdmin_model extends CI_Model
{
    public function addProfil($data)
    {
        $this->db->insert('profileadmin', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getProfileAdmin($id)
    {
        $this->db->select('id_profile, nom_admin, telephone_admin, adresse_admin, loaclisation_admin, image_admin, a.email_admin');
        $this->db->from('profileadmin');
        $this->db->join('administrateur as a', 'profileadmin.id_admin = a.id_admin');
        $this->db->where('profileadmin.id_admin', $id);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    public function getPassword($id){
        $this->db->select('password_admin');
        $this->db->from('administrateur');
        $this->db->where('id_admin', $id);
        $this->db->where('date_delete is null');

        $query = $this->db->get();
        $ret = $query->row();
        return $ret->password_admin;
    }
    public function changerMotPasse($id, $data){
        $this->db->where('id_admin', $id );
        $this->db->update('administrateur', $data);
        return true;
    }
    public function updateProfil($id, $data)
    {
        $this->db->where('id_admin', $id );
        $this->db->update('profileadmin', $data);
        return true;
    }
    public function updatePhotoProfil($data, $id)
    {
        $this->db->where('id_admin', $id );
        $this->db->update('profileadmin', $data);
        return true;
    }
}