<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Home_model');
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $this->load->view('pages/accueil_view');
    }

    public function EnvoiePlainte(){
        $this->form_validation->set_rules('nom', 'nom', 'trim|required');
        $this->form_validation->set_rules('genre', 'genre ', 'trim|required');
        $this->form_validation->set_rules('handicap', 'handicapé ', 'trim|required');
        $this->form_validation->set_rules('profession', 'profession', 'trim|required');
        $this->form_validation->set_rules('nationalite', 'nationalité', 'trim|required');
        $this->form_validation->set_rules('date_naissance', 'date naissance ', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse ', 'trim|required');
        $this->form_validation->set_rules('phone', 'telephone', 'trim|required');
        $this->form_validation->set_rules('statutPersonne', 'statut', 'trim|required');
        $this->form_validation->set_rules('statut', 'statut', 'trim|required');
        $this->form_validation->set_rules('docu', 'type document', 'trim|required');
        $this->form_validation->set_rules('tel_plainte', 'numéro téléphone', 'trim|required');
        $this->form_validation->set_rules('numero', 'numéro document', 'trim|required');
        $this->form_validation->set_rules('nom_plainte', 'nom plainte', 'trim|required');
        $this->form_validation->set_rules('adresse_plainte', 'adresse plainte', 'trim|required');
        $this->form_validation->set_rules('nature_plainte', 'nature de la plainte', 'trim|required');
        $this->form_validation->set_rules('langue_plainte', 'langue de la plainte', 'trim|required');
        //$this->form_validation->set_rules('plainte', 'plainte', 'trim|required');

        if($this->form_validation->run()==true)
        {
            //Get the last numero of folder
            $last_num = $this->Home_model->getLastNum();
            //true
            $nom_personne = $this->input->post('nom');
            $genre = $this->input->post('genre');
            $profession = $this->input->post('profession');
            $nationalite = $this->input->post('nationalite');
            $date_naissance = $this->input->post('date_naissance');
            $adresse_personne = $this->input->post('adresse');
            $email_personne = $this->input->post('your_email');
            $phone_personne = $this->input->post('phone');
            $statut_personne = $this->input->post('statutPersonne');
            $nom_plainte = $this->input->post('nom_plainte');
            $adresse_plainte = $this->input->post('adresse_plainte');
            $tel_plainte = $this->input->post('tel_plainte');
            $sujet_plainte = $this->input->post('nature_plainte');
            $langue_plainte = $this->input->post('langue_plainte');
            $description_plainte = $this->input->post('plainte');
            $handicap = $this->input->post('handicap');
            $statut = $this->input->post('statut');
            $type_document = $this->input->post('docu');
            $numero_document = $this->input->post('numero');
            $date_add = $this->getDatetimeNow();
            if ($last_num == false)
            {
                $numero = "1/".date("Y");
            }
            else{
                foreach ($last_num as $num) {
                    $num1 = $num->numero_dossier;
                }
                $num2 = $num1;
                $num3 = substr($num2, 0, -5);
                $num4 = $num3 +1;
                $numero = $num4."/".date("Y");
            }
            $num_dossier = $numero;

            //Data content
            $data = array(
                'nom_prenom' => $nom_personne,
                'profession' => $profession,
                'adresse' => $adresse_personne,
                'telephon' => $phone_personne,
                'email' => $email_personne,
                'nationalite' => $nationalite,
                'date_naissance' => $date_naissance,
                'sexe' => $genre,
                'statut_personne' => $statut_personne,
                'handicap' => $handicap,
                'statut' => $statut,
                'type_document' => $type_document,
                'numero_document' => $numero_document,
                'nom_plainte' => $nom_plainte,
                'tel_plainte' => $tel_plainte,
                'adresse_plainte' => $adresse_plainte,
                'nature_plainte' => $sujet_plainte,
                'langue_plainte' => $langue_plainte,
                'description_plainte' => $description_plainte,
                'date_ajout_plainte' => $date_add,
                'numero_dossier' => $num_dossier
            );
            //Send data to the model
            $result_plaint = $this->Home_model->addPlainte($data);
            //Redirect
            if($result_plaint == TRUE)
            {
                $upload_files = $this->upload_files($num_dossier);
                if($upload_files == TRUE)
                {
                    //Send email to the secretariat (notification)
                    $nom_type = "Secretariat";
                    $email_secretariat = $this->Home_model->getEmailSecretariat($nom_type);
                    $data['email_secretariat'] = $email_secretariat;

                    foreach ($email_secretariat as $key => $email) {
                        $secretaireEmail = $email->email;
                    }

                    $this->sendemail($secretaireEmail);
                }
                
            }
            else{
                $this->erreur();
            }
        }
        else{
            //False
            $this->index();
        }
    }

    public function upload_files($token){
        $result =FALSE;
        $folder = 'pieces_joint';

        if (!is_dir('./uploads/' . $folder)) {
            mkdir('./uploads/' . $folder, 0777, TRUE);
        }
        $this->load->library('upload');
        for ($i = 0; $i<count($_FILES) && $i<10; $i++){
            $_FILES['userfile']['name']     = $_FILES['file_upload']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['file_upload']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['file_upload']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['file_upload']['size'][$i];

            $config = array(
                'allowed_types' => 'jpg|pdf|png|jpeg|JPG|JPEG|PNG|PDF',
                'max_size'      => 5000,
                'overwrite'     => true,     
                'remove_spaces' => TRUE, 
                'upload_path' =>'./uploads/pieces_joint/'                 
            );
            $this->upload->initialize($config);
        
            if ($this->upload->do_upload('userfile')) {
                $final_files_data = $this->upload->data();
                $this->Home_model->piecesJoints($final_files_data['file_name'], $token);
                $result = TRUE;
            } else {
                $result = $this->upload->display_errors();
            }
        }
        return $result;
    }
       

    public function sendemail($secretaireEmail)
    {
        # Config...
        /*$config = array(
            'protocol'  =>  'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_timeout' => 7,
            'smtp_user' => 'formulaire.plainte@gmail.com',
            'smtp_pass' => 'formcndh',
            'mailtype'  => 'html',
            'charset'   => 'utf8',
            'wordwrap'  => TRUE
        );*/
        $config = array(
            'protocol'  =>  'smtp',
            'smtp_host' => 'webmail.ansie.dj',
            'smtp_port' => 587,
            'smtp_user' => 'test1@ansie.dj',
            'smtp_pass' => 'password@1',
            'mailtype'  => 'html',
            'charset'   => 'utf8',
            'wordwrap'  => TRUE
        );
        $data['contente']="Commission Nationale des Droits de l'Homme";
        $message = $this->load->view('pages/email_view',$data,true);
        $sujet = "Demande de plainte";


        $this->email->initialize($config);
        $this->load->library('email', $config);
        $this->email->from('formulaire.plainte@gmail.com');
        $this->email->to($secretaireEmail);
        $this->email->subject($sujet);
        $this->email->message($message);
        $this->email->set_newline(" \ r \ n ");
        $result = $this->email->send();
        if($result){
            $this->success();
        }
        else{
            var_dump($this->email->print_debugger());die();
            echo $this->email->print_debugger();
        }

    }

    public function success()
    {
        $this->load->view('pages/success_view');
    }
    public function erreur()
    {
        $this->load->view('pages/error_view');
    }
}
