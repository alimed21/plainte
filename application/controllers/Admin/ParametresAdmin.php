<?php


class ParametresAdmin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/ProfileAdmin_model');
        $this->load->model('Admin/Login_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('id_admin')) {
            redirect('Admin/LoginAdmin');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $profileUser = $this->ProfileAdmin_model->getProfileAdmin($id_admin);
        $data['profileUser'] = $profileUser;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/Parametres_view', $data);
        $this->load->view('administration/templates/footer');
    }

    public function motPasse(){
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;


        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/ModificationMotPasse_view', $data);
        $this->load->view('administration/templates/footer');
    }

    public function modificationMotPasse()
    {
        $this->form_validation->set_rules('ancienpass', "ancien mot de passe", 'trim|required');
        $this->form_validation->set_rules('nouveaupass', 'nouveau mot de passe', 'trim|required');
        $this->form_validation->set_rules('cnfpass', 'confirmation du nouveau mot de passe', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            $id = $this->session->userdata('id_admin');
            $lastpassword = $this->ProfileAdmin_model->getPassword($id);

            $ancienPass = md5($this->input->post('ancienpass'));
            $nouveaupass = md5($this->input->post('nouveaupass'));
            $cnfpass    = md5($this->input->post('cnfpass'));

            if($lastpassword == $ancienPass)
            {
                if($nouveaupass == $cnfpass)
                {
                    $data = array(
                        'password_admin' => $nouveaupass
                    );

                    $nouveauPass = $this->ProfileAdmin_model->changerMotPasse($id, $data);

                    if ($nouveauPass = true)
                    {
                        redirect('Admin/LoginAdmin/logout');
                    }
                    else{
                        $this->session->set_flashdata('error', 'Veuillez réessayer.');
                        redirect('Admin/ParametresAdmin/motPasse');
                    }
                }
                else{
                    $this->session->set_flashdata('error', 'Les deux mots de passe ne sont pas identiques.');
                    redirect('Admin/ParametresAdmin/motPasse');
                }
            }
            else{
                $this->session->set_flashdata('error', 'Votre ancien mot de passe est incorrect.');
                redirect('Admin/ParametresAdmin/motPasse');
            }
        }
        else{
            $this->motPasse();
        }
    }
}