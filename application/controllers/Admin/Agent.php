<?php


class Agent extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/Plainte_model');
        $this->load->model('Admin/TypeCompte_model');
        $this->load->model('Admin/Login_model');
        $this->load->library('form_validation');
        require_once('./assets/vendor/autoload.php');
        require_once(APPPATH . "third_party/phpmailer/PHPMailerAutoload.php");
        if (!$this->session->userdata('id_user')) {
            redirect('Admin/Login');
        }
        if($this->session->userdata('nom_type') != "Agent")
        {
            redirect('Admin/Login/logout');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesAffecter = $this->Plainte_model->getPlainteAffecter($id);
        $data['plaintesAffecter'] = $plaintesAffecter;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listPlaintesAffecter_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function pdf($numeroDossier)
    {
        //Path to download the pdf file
        $form = $this->Plainte_model->getDossierPlainte($numeroDossier);
        $data['form'] = $form;
        foreach($form as $d)
        {
            $num = $d->numero_dossier;
        }
        $this->load->view('admin/pages/report_page_view', $data, true);
        $trait = "_";
        $date =  preg_replace('/\s+/', '', date('d-m-Y'));
        $nom_user =  preg_replace('/\s+/', '', $num);
        $name_file = $nom_user.$trait.$date.'.pdf';
        //var_dump($name_file);die();
        $html = $this->load->view('admin/pages/report_page_view', $data, true);
        $mpdf = new \Mpdf\Mpdf(['format' => 'A4-P', 'tempDir' => './uploads/pdf/tmp']);
        $mpdf->SetHTMLFooter('<table width="100%" style="font-size: 8pt; color: #000000;">
        <tfoot>
        <tr>
            <td style="width: 70%;border: none;background-color: white;font-style: italic;">Ce document a été généré le {DATE j/m/Y H:i:s}</td>
            <td style="width: 20%;border: none;background-color: white;font-style: italic;" align="center">{PAGENO}/{nbpg}</td>
          </tr>
        </tfoot>

        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($name_file, 'I');

    }

    public function affectPlaint()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesNumero = $this->Plainte_model->getNumeroPlainteAffecte();
        $data['plaintesNumero'] = $plaintesNumero;

        /*$typePlaintes = $this->TypeCompte_model->getAllTypePlainte();
        $data['typePlaintes'] = $typePlaintes;*/

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/PlaintesAffecter_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function Affectation()
    {
        $this->form_validation->set_rules('plainte', 'plainte ', 'trim|required');
        $this->form_validation->set_rules('commentaire', 'commentaire', 'trim|required');


        if($this->form_validation->run()==true)
        {
            $plainte = $this->input->post('plainte');
            $commentaire = $this->input->post('commentaire');
            $id_agent = $this->session->userdata('id_user');
            $date_affect = $this->getDatetimeNow();

            $result_function = $this->affectSG($plainte);

            if ($result_function = true)
            {
                $data = array(
                    'id_agent' => $id_agent,
                    'id_plainte ' => $plainte,
                    'commentaire' => $commentaire,
                    'date_affect' => $date_affect
                );

                $resutl_affect = $this->Plainte_model->AffecterPlainteSG($data);

                if ($resutl_affect = true)
                {
                    $affectPlainte = $this->plainteAffect($plainte);
                    if($affectPlainte = true){
                        $this->sendEmail();
                    }
                    else{
                        //False
                        $this->session->set_flashdata('error', 'Veuillez réessayer.');
                        redirect('Admin/Agent/affectPlaint');
                    }
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Agent/affectPlaint');
                }
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Agent/affectPlaint');
            }
        }
        else{
            $this->affectPlaint();
        }
    }

    public function plainteAffect($id)
    {
        $values = 1;
        $data = array(
            'plainte_affect' => $values
        );

        $resutl = $this->Plainte_model->updatePlainteAffect($id, $data);

        if ($resutl = true)
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function affectSG($id)
    {
        $values = 1;
        $data = array(
            'affect_sg' => $values
        );

        $resutl = $this->Plainte_model->updatePlainte($id, $data);

        if ($resutl = true)
        {
           return true;
        }
        else{
            return false;
        }
    }

    public function sendemail()
    {
        # Config...
        $config = array(
            'smtp_crypto' => 'tls',
            'protocol'  =>  'mail',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => 'formulaire.plainte@gmail.com',
            'smtp_pass' => 'formcndh',
            'mailtype'  => 'html',
            'charset'   => 'utf8',
            'wordwrap'  => TRUE
        );

        $nom_type = "Secrétaire Général";
        $emailSG = $this->Plainte_model->getEmailSG($nom_type);
        $data['emailSG'] = $emailSG;

        foreach($emailSG as $sg)
        {
            $email = $sg->email;
        }

        $data['contente']="Commission Nationale des Droits de l'Homme";
        $message = $this->load->view('admin/pages/email2_view',$data,true);
        $sujet = "Affectation de plainte";


        $this->email->initialize($config);
        $this->load->library('email', $config);
        $this->email->from('formulaire.plainte@gmail.com');
        $this->email->to($email);
        $this->email->subject($sujet);
        $this->email->message($message);
        $this->email->set_newline("\r\n");
        $result = $this->email->send();
        if($result){
            $action = "Affectation plainte SG";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Affectation SG réussi.');
            redirect('Admin/Agent/');
        }
        else{
            $this->session->set_flashdata('error', "Affectation n'a pas réussi.");
            redirect('Admin/Agent/affectPlaint');
        }

    }
    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}