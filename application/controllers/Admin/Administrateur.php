<?php


class Administrateur extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/TypeCompte_model');
        $this->load->model('Admin/Utilisateurs_model');
        $this->load->library('form_validation');
        if(!$this->session->userdata('id_admin'))
        {
            redirect('Admin/LoginAdmin');
        }
        //if($this->session->userdata(''))
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }


    public function index()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $Users = $this->Utilisateurs_model->getAllUser();
        $data['Users'] = $Users;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/listUtilisateurs_view', $data);
        $this->load->view('administration/templates/footer');
    }

    public function ajoutUtilisateur()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/ajoutUtilisateur_view', $data);
        $this->load->view('administration/templates/footer');
    }

    public function VerifyForm()
    {
        $this->form_validation->set_rules('username', "nom d'utilisateur", 'trim|required');
        $this->form_validation->set_rules('password', 'mot de passe ', 'trim|required');
        $this->form_validation->set_rules('cnfpassword', 'confirmation du mot de passe', 'trim|required');
        $this->form_validation->set_rules('email', 'email ', 'trim|required|valid_email|is_unique[utilisateurs.email]');
        $this->form_validation->set_rules('type', 'type', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            //True
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $cnfpass  = md5($this->input->post('cnfpassword'));
            $email    = $this->input->post('email');
            $type     = $this->input->post('type');
            $id_admin = $this->session->userdata('id_admin');
            $date_add = $this->getDatetimeNow();

            if($password == $cnfpass)
            {
                $data = array(
                    'username' => $username,
                    'password' => $password,
                    'email '   => $email,
                    'id_type'  => $type,
                    'id_admin_add' => $id_admin,
                    'date_add' => $date_add
                );

                $addUser = $this->Utilisateurs_model->addUser($data);

                if ($addUser = true)
                {
                    $action = "Ajout utilisateur";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Ajout utilisateur réussi');
                    redirect('Admin/Administrateur');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Administrateur/ajoutUtilisateur');
                }

            }
            else{
                $this->session->set_flashdata('error', 'Les deux mots de passes ne sont pas identités.');
                redirect('Admin/Administrateur/ajoutUtilisateur');
            }
        }
        else{
            $this->ajoutUtilisateur();
        }
    }

    public function SupprimerUtilisateur($id)
    {
        $data = array(
            'id_admin_delete' => $this->session->userdata('id_admin'),
            'date_delete' => $this->getDatetimeNow()
        );

        $deleteUser = $this->Utilisateurs_model->suppressionUtilisateur($id, $data);

        if ($deleteUser = true)
        {
            $action = "Suppression d'un utilisateur";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression utilisateur réussi');
            redirect('Admin/Administrateur/');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Administrateur/');
        }
    }
    /** Type Compte**/

    public function ListeTypeCompte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typeComptes = $this->TypeCompte_model->getAllType();
        $data['typeComptes'] = $typeComptes;


        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/listTypeCompte_view', $data);
        $this->load->view('administration/templates/footer');
    }

    public function ajoutTypeCompte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;


        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/ajoutTypeCompte_view');
        $this->load->view('administration/templates/footer');
    }

    public function VerifyFormType()
    {
        $this->form_validation->set_rules('type', "libellé type", 'trim|required');

        if ($this->form_validation->run() == true) {
            //True
            $type = $this->input->post('type');

            $data = array(
                'nom_type' => $type,
                'id_admin_add_type' => $this->session->userdata('id_admin'),
                'date_add_type' => $this->getDatetimeNow()
            );

            $addTypeCompte = $this->TypeCompte_model->addTypeCompte($data);

            if ($addTypeCompte = true)
            {
                $action = "Ajout type compte";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', 'Ajout type compte réussi');
                redirect('Admin/Administrateur/ListeTypeCompte');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Administrateur/ajoutTypeCompte');
            }
        }
        else{
            $this->ajoutTypeCompte();
        }
    }
    public function SupprimerTypeCompte($id)
    {
        $data = array(
            'id_admin_delete_type' => $this->session->userdata('id_admin'),
            'date_delete_type' => $this->getDatetimeNow()
        );

        $deleteTypeCompte = $this->TypeCompte_model->suppressionTypeCompte($id, $data);

        if ($deleteTypeCompte = true)
        {
            $action = "Suppression type de compte";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression type de compte réussi');
            redirect('Admin/Administrateur/ListeTypeCompte');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Administrateur/ListeTypeCompte');
        }
    }

    /** Type de plainte */
    public function ajoutTypePlainte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;


        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/ajoutTypePlainte_view');
        $this->load->view('administration/templates/footer');
    }

    public function VerifyFormTypePlainte()
    {
        $this->form_validation->set_rules('type', "libellé de la convention", 'trim|required');
        $this->form_validation->set_rules('abbr', "abbreviation de la convention", 'trim|required');

        if ($this->form_validation->run() == true) {
            //True
            $convention = $this->input->post('type');
            $abbr = $this->input->post('abbr');

            $data = array(
                'nom_type_plainte' => $convention,
                'nom_court' => $abbr,
                'id_admin_add' => $this->session->userdata('id_admin'),
                'date_add' => $this->getDatetimeNow()
            );

            $addTypeCompte = $this->TypeCompte_model->addConvention($data);

            if ($addTypePlainte = true)
            {
                $action = "Ajout type plainte";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', "Ajout d'une convention réussi");
                redirect('Admin/Administrateur/ListeTypePlainte');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Administrateur/ajoutTypePlainte');
            }
        }
        else{
            $this->ajoutTypePlainte();
        }
    }
    public function ListeTypePlainte()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $typePlaintes = $this->TypeCompte_model->getAllTypePlainte();
        $data['typePlaintes'] = $typePlaintes;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/listTypePlainte_view', $data);
        $this->load->view('administration/templates/footer');
    }
    public function SupprimerTypePlainte($id)
    {
        $data = array(
            'id_admin_delete' => $this->session->userdata('id_admin'),
            'date_delete' => $this->getDatetimeNow()
        );

        $deleteTypePlainte = $this->TypeCompte_model->suppressionTypePlainte($id, $data);

        if ($deleteTypePlainte = true)
        {
            $action = "Suppression type de plainte";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression type de plainte réussi');
            redirect('Admin/Administrateur/ListeTypePlainte');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Administrateur/ListeTypePlainte');
        }
    }
    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_admin'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }

}