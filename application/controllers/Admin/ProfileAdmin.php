<?php


class ProfileAdmin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/ProfileAdmin_model');
        $this->load->model('Admin/Login_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('id_admin')) {
            redirect('Admin/LoginAdmin');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $profileAdmin = $this->ProfileAdmin_model->getProfileAdmin($id_admin);
        $data['profileAdmin'] = $profileAdmin;

        if ($profileAdmin == TRUE)
        {
            $this->load->view('administration/templates/header', $data);
            $this->load->view('administration/pages/Profile_view', $data);
            $this->load->view('administration/templates/footer');
        }
        else{
            $this->ajoutProfile();
        }
    }

    public function ajoutProfile()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/ajoutProfile_view');
        $this->load->view('administration/templates/footer');
    }

    public function VerifyForm()
    {
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('localisation', 'localisation', 'trim|required');

        //Config image
        $config['upload_path'] = './uploads/image_admin';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 1024;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->form_validation->run() == true)
        {
            //True
            if (!$this->upload->do_upload('userfile')) {

                $id_admin = $this->session->userdata('id_admin');

                $infoUser = $this->Home_model->getInfoAdmin($id_admin);
                $data['infoUser'] = $infoUser;

                $this->load->view('administration/templates/header', $data);
                $this->load->view('administration/pages/ajoutProfile_view');
                $this->load->view('administration/templates/footer');
            }
            else{
                $full_path = "uploads/image_admin/" . $this->upload->data('file_name');
                $nom = $this->input->post('nom');
                $telephone = $this->input->post('telephone');
                $adresse    = $this->input->post('adresse');
                $localisation     = $this->input->post('localisation');

                $data = array(
                    'id_admin' => $this->session->userdata('id_admin'),
                    'nom_admin' => $nom,
                    'telephone_admin' => $telephone,
                    'adresse_admin '   => $adresse,
                    'loaclisation_admin'  => $localisation,
                    'image_admin' => strtolower($full_path)
                );

                $addProfile = $this->ProfileAdmin_model->addProfil($data);

                if ($addProfile = true)
                {
                    $this->session->set_flashdata('sucess', 'Ajout profile réussi');
                    redirect('Admin/ProfileAdmin/');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/ProfileAdmin/ajoutProfile');
                }
            }
        }
        else{
            $this->ajoutProfile();
        }
    }

    public function modifierProfile()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;

        $infoProfile = $this->ProfileAdmin_model->getProfileAdmin($id_admin);
        $data['infoProfile'] = $infoProfile;

        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/modifierProfile_view');
        $this->load->view('administration/templates/footer');
    }

    public function modificationVerification()
    {
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('localisation', 'localisation', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            $nom = $this->input->post('nom');
            $telephone = $this->input->post('telephone');
            $adresse    = $this->input->post('adresse');
            $localisation     = $this->input->post('localisation');
            $id = $this->session->userdata('id_admin');

            $data = array(
                'nom_admin' => $nom,
                'telephone_admin' => $telephone,
                'adresse_admin'   => $adresse,
                'loaclisation_admin'  => $localisation
            );

            $updateProfile = $this->ProfileAdmin_model->updateProfil($id, $data);

            if ($updateProfile = true)
            {
                redirect('Admin/ProfileAdmin/');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Profile/modifierProfile');
            }
        }
        else{
            $this->modifierProfile();
        }
    }

    public function modifierPhotoProfile()
    {
        $id_admin = $this->session->userdata('id_admin');

        $infoUser = $this->Home_model->getInfoAdmin($id_admin);
        $data['infoUser'] = $infoUser;


        $this->load->view('administration/templates/header', $data);
        $this->load->view('administration/pages/modifierPhoto_view');
        $this->load->view('administration/templates/footer');
    }

    public function VerifyModificationPhoto()
    {
        //Config image
        $config['upload_path'] = './uploads/image_admin';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 1024;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            $id = $this->session->userdata('id_admin');

            $infoUser = $this->Home_model->getInfoAdmin($id);
            $data['infoUser'] = $infoUser;


            $data['error_message'] = $this->upload->display_errors();
            $this->load->view('administration/templates/header', $data);
            $this->load->view('administration/pages/modifierPhoto_view');
            $this->load->view('administration/templates/footer');
        }
        else{
            $full_path = "uploads/image_admin/" . $this->upload->data('file_name');
            $id =  $this->session->userdata('id_admin');
            $data = array(
                'image_admin' => strtolower($full_path)
            );

            $updateProfile = $this->ProfileAdmin_model->updatePhotoProfil($data, $id);

            if ($updateProfile = true)
            {
                redirect('Admin/ParametresAdmin');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/ProfileAdmin/ajoutProfile');
            }
        }
    }

}