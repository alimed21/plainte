<?php


class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/Profile_model');
        $this->load->model('Admin/Plainte_model');
        $this->load->model('Admin/Login_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('id_user')) {
            redirect('Admin/Login');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $profileUser = $this->Profile_model->getProfileUser($id);
        $data['profileUser'] = $profileUser;

        if ($profileUser == TRUE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/pages/Profile_view', $data);
            $this->load->view('admin/templates/footer');
        }
        else{
            $this->ajoutProfile();
        }
    }

    public function ajoutProfile()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutProfile_view');
        $this->load->view('admin/templates/footer');
    }

    public function VerifyForm()
    {
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('localisation', 'localisation', 'trim|required');

        //Config image
        $config['upload_path'] = './uploads/imague_user';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->form_validation->run() == true)
        {
            //True
            if (!$this->upload->do_upload('userfile')) {
                $id = $this->session->userdata('id_user');

                $infoUser = $this->Home_model->getInfoUser($id);
                $data['infoUser'] = $infoUser;

                $data['error_message'] = $this->upload->display_errors();

                $this->load->view('admin/templates/header', $data);
                $this->load->view('admin/pages/ajoutProfile_view', $data);
                $this->load->view('admin/templates/footer');
            }
            else{
                $full_path = "uploads/imague_user/" . $this->upload->data('file_name');
                $nom = $this->input->post('nom');
                $telephone = $this->input->post('telephone');
                $adresse    = $this->input->post('adresse');
                $localisation     = $this->input->post('localisation');

                $data = array(
                    'id_user' => $this->session->userdata('id_user'),
                    'nom_user' => $nom,
                    'telephone_user' => $telephone,
                    'adresse_user '   => $adresse,
                    'loaclisation_user'  => $localisation,
                    'image_user' => strtolower($full_path)
                );

                $addProfile = $this->Profile_model->addProfil($data);

                if ($addProfile = true)
                {
                    $action = "Ajout profile";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Ajout profile réussi');
                    redirect('Admin/Profile/');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Profile/ajoutProfile');
                }
            }
        }
        else{
            $this->ajoutProfile();
        }
    }

    public function modifierProfile()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $infoProfile = $this->Profile_model->getProfileUser($id);
        $data['infoProfile'] = $infoProfile;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/modifierProfile_view');
        $this->load->view('admin/templates/footer');
    }

    public function modificationVerification()
    {
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('localisation', 'localisation', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            $nom = $this->input->post('nom');
            $telephone = $this->input->post('telephone');
            $adresse    = $this->input->post('adresse');
            $localisation     = $this->input->post('localisation');
            $id = $this->session->userdata('id_user');

            $data = array(
                'nom_user' => $nom,
                'telephone_user' => $telephone,
                'adresse_user '   => $adresse,
                'loaclisation_user'  => $localisation
            );

            $updateProfile = $this->Profile_model->updateProfil($id, $data);

            if ($updateProfile = true)
            {
                $action = "Modification profile";
                $this->histoirque($action);
                redirect('Admin/Profile/');
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Profile/modifierProfile');
            }
        }
        else{
            $this->modifierProfile();
        }
    }

    public function modifierPhotoProfile()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/modifierPhoto_view');
        $this->load->view('admin/templates/footer');
    }

    public function VerifyModificationPhoto()
    {
        //Config image
        $config['upload_path'] = './uploads/imague_user';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            $id = $this->session->userdata('id_user');

            $infoUser = $this->Home_model->getInfoUser($id);
            $data['infoUser'] = $infoUser;

            $data['error_message'] = $this->upload->display_errors();

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/pages/modifierPhoto_view', $data);
            $this->load->view('admin/templates/footer');
        }
        else{
                $full_path = "uploads/imague_user/" . $this->upload->data('file_name');
                $id =  $this->session->userdata('id_user');
                $data = array(
                    'image_user' => strtolower($full_path)
                );

                $updateProfile = $this->Profile_model->updatePhotoProfil($data, $id);

                if ($updateProfile = true)
                {
                    $action = "Modification photo de profile";
                    $this->histoirque($action);
                    redirect('Admin/Parametres');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Profile/ajoutProfile');
                }
            }
    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}