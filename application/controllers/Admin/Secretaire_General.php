<?php


class Secretaire_General extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Plainte_model');
        $this->load->model('Admin/TypeCompte_model');
        $this->load->library('form_validation');
        require_once ('./assets/vendor/autoload.php');
        require_once(APPPATH."third_party/phpmailer/PHPMailerAutoload.php");
        if(!$this->session->userdata('id_user'))
        {
            redirect('Admin/Login');
        }
        if($this->session->userdata('nom_type') != "Secrétaire Général")
        {
            redirect('Admin/Login/logout');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function index()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesSG = $this->Plainte_model->getAllPlainteSG();
        $data['plaintesSG'] = $plaintesSG;

        $piecesJoints = $this->Home_model->getAllPieceJoint();
        $data['piecesJoints'] = $piecesJoints;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listPlaintesSG_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function pdf($numeroDossier)
    {
        if($numeroDossier == "")
        {
            redirect('Admin/Error/');
        }
        //Path to download the pdf file
        $form = $this->Plainte_model->getDossierPlainte($numeroDossier);
        $data['form'] = $form;
        foreach($form as $d)
        {
            $num = $d->numero_dossier;
        }
        $this->load->view('admin/pages/report_page_view', $data);
        /*
        $trait = "_";
        $date =  preg_replace('/\s+/', '', date('d-m-Y'));
        $nom_user =  preg_replace('/\s+/', '', $num);
        $name_file = $nom_user.$trait.$date.'.pdf';
        //var_dump($name_file);die();
        $html = $this->load->view('admin/pages/report_page_view', $data, true);
        $mpdf = new \Mpdf\Mpdf(['format' => 'A4-P', 'tempDir' => './uploads/pdf/tmp']);
        $mpdf->SetHTMLFooter('<table width="100%" style="font-size: 8pt; color: #000000;">
        <tfoot>
        <tr>
            <td style="width: 70%;border: none;background-color: white;font-style: italic;">Ce document a été généré le {DATE j/m/Y H:i:s}</td>
            <td style="width: 20%;border: none;background-color: white;font-style: italic;" align="center">{PAGENO}/{nbpg}</td>
          </tr>
        </tfoot>

        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($name_file, 'I');
*/
    }

    public function plainteDecision($id_plainte)
    {
        $id = $this->session->userdata('id_user');

        $data['id_plainte'] = $id_plainte;

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesSG = $this->Plainte_model->getAllPlainteSG();
        $data['plaintesSG'] = $plaintesSG;

        $conventions = $this->Plainte_model->getAllConvention();
        $data['conventions'] = $conventions;

        //var_dump($conventions);die;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/decisionPlaintesSG_view', $data);
        $this->load->view('admin/templates/footer');
    }
    public function formDecision()
    {
        $this->form_validation->set_rules('decision', "nom", 'trim|required');
        //$this->form_validation->set_rules('commentaire', 'téléphone', 'trim|required');

        $id_plainte = $this->input->post('id_plainte');

        $id = $this->session->userdata('id_user');

        if($this->form_validation->run() == true)
        {
            //true
            $decison = $this->input->post('decision');


            if($decison == "recevable")
            {
                $type_convention = $this->input->post('convention');
                $data = array(
                    'id_plainte' => $id_plainte,
                    'decision' => $decison,
                    'type_convention' => $type_convention,
                    'date_decision' => $this->getDatetimeNow(),
                    'id_sg' => $id
                );

                $decision_requet = $this->Plainte_model->decisionPlainteSG($data);

                if($decision_requet == TRUE){
                    $this->plainteAcceptSG($id_plainte, $decison);
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Secretaire_General/formDecision');
                }
            }
            else{
                $commentaire = $this->input->post('commentaire');
                $data = array(
                    'id_plainte' => $id_plainte,
                    'decision' => $decison,
                    'commentaire_sg' => $commentaire,
                    'date_decision' => $this->getDatetimeNow(),
                    'id_sg' => $id
                );

                $decision_requet = $this->Plainte_model->decisionPlainteSG($data);

                if($decision_requet == TRUE){
                    $this->plainteRefusSG($id_plainte, $decison);
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/Secretaire_General/formDecision');
                }
            }

        }
        else{
            //false
            $this->plainteDecision($id_plainte);
        }
    }
    public function plainteAcceptSG($id_plainte, $decison)
    {
        $data = array(
            'decision_sg' => $decison
        );

      
        $updateDecision = $this->Plainte_model->plainteAccepterSG($id_plainte, $data);
        //var_dump($updateDecision);die;
        if ($updateDecision = true)
        {
            $action = "Plainte recevable";
            $this->histoirque($action);
            $this->Valide($id_plainte);
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/');
        }
    }
    public function plainteRefusSG($id_plainte, $decison)
    {
        $data = array(
            'decision_sg' => $decison
        );
        $updateDecision = $this->Plainte_model->plainteAccepterSG($id_plainte, $data);

        if ($updateDecision = true)
        {
            $action = "Plainte non-recevable";
            $this->histoirque($action);
            $this->nonValide($id_plainte);
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/');
        }
    }
    public function Valide($id)
    {
        $valid = 1;
        $data = array(
            'statut_sg' => $valid
        );
        $result = $this->Plainte_model->plainteAccepter($id, $data);

        if ($result = true)
        {
            $action = "Plainte recevable";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Validation réussi.');
            redirect('Admin/Secretaire_General/');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/');
        }
    }

    public function nonValide($id)
    {
        $valid = 0;
        $data = array(
            'statut_sg' => $valid
        );
        $result = $this->Plainte_model->plainteAccepter($id, $data);

        if ($result = true)
        {
            $action = "Plainte irrecevable";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Validation réussi.');
            redirect('Admin/Secretaire_General/');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/');
        }
    }

    public function Affecter()
    {
        $id = $this->session->userdata('id_user');
        $decision = "recevable";

        $typeUser = "Agent";

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesNumero = $this->Plainte_model->getNumeroPlainte($decision);
        $data['plaintesNumero'] = $plaintesNumero;

        $Agents =  $this->Home_model->getAllAgent($typeUser);
        $data['Agents'] = $Agents;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/Fonction_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function Affectation()
    {
        $this->form_validation->set_rules('agent', 'agent', 'trim|required');
        $this->form_validation->set_rules('plainte', 'plainte ', 'trim|required');

        if($this->form_validation->run()==true)
        {
            $agent = $this->input->post('agent');
            $plainte = $this->input->post('plainte');

            $data = array(
                'agent_affecter' => $agent
            );

            $resutl = $this->Plainte_model->AffecterPlainte($plainte, $data);

            if ($result = true)
            {
                $this->sendemailAgent($agent);
                
            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Admin/Accueil/Affecter');
            }
        }
        else{
            $this->Affecter();
        }
    }

    public function sendemailAgent($agent)
    {
        # Config...
        $config = array(
            'smtp_crypto' => 'tls',
            'protocol'  =>  'mail',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => 'formulaire.plainte@gmail.com',
            'smtp_pass' => 'formcndh',
            'mailtype'  => 'html',
            'charset'   => 'utf8',
            'wordwrap'  => TRUE
        );


        $emailAgent = $this->Plainte_model->getEmailAgent($agent);
        $data['emailAgent'] = $emailAgent;

        foreach($emailAgent as $agent)
        {
            $email = $agent->email;
        }

        $data['contente']="Commission Nationale des Droits de l'Homme";
        $message = $this->load->view('admin/pages/email_view',$data,true);
        $sujet = "Affectation de plainte";


        $this->email->initialize($config);
        $this->load->library('email', $config);
        $this->email->from('formulaire.plainte@gmail.com');
        $this->email->to($email);
        $this->email->subject($sujet);
        $this->email->message($message);
        $this->email->set_newline("\r\n");
        $result = $this->email->send();
        if($result){
            $action = "Affectation plainte";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Affectation réussi.');
            redirect('Admin/Secretaire_General/Affecter');
        }
        else{
            $this->session->set_flashdata('error', "Affectation n'a pas réussi.");
            redirect('Admin/Secretaire_General/Affecter');
        }

    }

    public function categorie(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plaintesSG_valid = $this->Plainte_model->getAllPlainteSG_Valid();
        $data['plaintesSG_valid'] = $plaintesSG_valid;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/categoriePlainte_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function plainteSimple($id)
    {
        $type_plainte = 'simple';

        $data = array(
            'type_plainte' => $type_plainte
        );

        $type_plaintes = $this->Plainte_model->TypePlainte($id, $data);

        if($type_plaintes == TRUE)
        {
            $plainteType = $this->Plainte_model->TypePlainte2($id, $data);

            $action = "Plainte simple";
            $this->histoirque($action);
            $this->sendemail();
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/categorie');
        }
    }

    public function plainteComplexe($id)
    {
        $type_plainte = 'complexe';

        $data = array(
            'type_plainte' => $type_plainte
        );

        $type_plaintes = $this->Plainte_model->TypePlainte($id, $data);

        if($type_plaintes == TRUE)
        {
            $action = "Plainte complexe";
            $this->histoirque($action);
            $this->sendemail();
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Admin/Secretaire_General/categorie');
        }
    }

    public function sendemail()
    {
        # Config...
        $config = array(
            'smtp_crypto' => 'tls',
            'protocol'  =>  'mail',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => 'formulaire.plainte@gmail.com',
            'smtp_pass' => 'formcndh',
            'mailtype'  => 'html',
            'charset'   => 'utf8',
            'wordwrap'  => TRUE
        );

        $nom_type = "Président";
        $emailPresident = $this->Plainte_model->getEmailPresident($nom_type);
        $data['emailPresident'] = $emailPresident;

        foreach($emailPresident as $president)
        {
            $email = $president->email;
        }

        $data['contente']="Commission Nationale des Droits de l'Homme";
        $message = $this->load->view('admin/pages/emailPresident_view',$data,true);
        $sujet = "Décision de plainte";


        $this->email->initialize($config);
        $this->load->library('email', $config);
        $this->email->from('formulaire.plainte@gmail.com');
        $this->email->to($email);
        $this->email->subject($sujet);
        $this->email->message($message);
        $this->email->set_newline("\r\n");
        $result = $this->email->send();
        if($result){
            $this->session->set_flashdata('success', 'Validation réussi.');
            redirect('Admin/Secretaire_General/categorie');
        }
        else{
            $this->session->set_flashdata('error', "La validation n'a pas réussi.");
            redirect('Admin/Secretaire_General/categorie');
        }

    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}