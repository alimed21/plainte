<?php


class President extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Home_model');
        $this->load->model('Admin/President_model');
        $this->load->model('Admin/Login_model');
        $this->load->model('Admin/Plainte_model');
        $this->load->library('form_validation');
        require_once ('./assets/vendor/autoload.php');
        require_once(APPPATH."third_party/phpmailer/PHPMailerAutoload.php");
        if(!$this->session->userdata('id_user'))
        {
            redirect('Admin/Login');
        }
        if($this->session->userdata('nom_type') != "Président")
        {
            redirect('Admin/Login/logout');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function chart()
    {
        $id = $this->session->userdata('id_user');

        $recevable = "recevable";

        $rejecter = "irrecevable";

        $simple = "simple";

        $complexe = "complexe";

        $homme = 'h';

        $femme = 'f';

        $enfant = 'e';

        $handicap = 'oui';
        $nonhandicap = 'non';

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $nbrePlaintes = $this->President_model->countAllPlaint();
        $data['nbrePlaintes'] = $nbrePlaintes;

        $plaintesRecevable = $this->President_model->countPlaintesRecevable($recevable);
        $data['plaintesRecevable'] = $plaintesRecevable;

        $plaintesSimple = $this->President_model->countPlaintesSimple($simple);
        $data['plaintesSimple'] = $plaintesSimple;

        $plaintesComplexe = $this->President_model->countPlaintesComplexe($complexe);
        $data['plaintesComplexe'] = $plaintesComplexe;

        //Plainte par mois
        $plaintesMois = $this->President_model->countPlaintesParMois();
        $data['plaintesMois'] = $plaintesMois;

        //var_dump($plaintesMois);die;

        //Type de compte et nbre d'utilisateur
        $nbretype = $this->President_model->typeUser();
        $data['nbretype'] = $nbretype;

        //Liste utilisateurs#
        $users = $this->President_model->ListUsers();
        $data['users'] = $users;

        //Plainte genre
        $plainteGenreH = $this->President_model->genreHomme($homme);
        $data['plainteGenreH'] = $plainteGenreH;

        $plainteGenreF = $this->President_model->genreFemme($femme);
        $data['plainteGenreF'] = $plainteGenreF;

        $plainteGenreE = $this->President_model->genreEnfant($enfant);
        $data['plainteGenreE'] = $plainteGenreE;

        $plainteHandicap = $this->President_model->handicap($handicap);
        $data['plainteHandicap'] = $plainteHandicap;

        $plainteNonHandicap = $this->President_model->Nonhandicap($nonhandicap);
        $data['plainteNonHandicap'] = $plainteNonHandicap;

        //Plainte rejeter
        $plaintesRejeter = $this->President_model->countPlaintesRejeter($rejecter);
        $data['plaintesRejeter'] = $plaintesRejeter;



        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/chart', $data);
        $this->load->view('admin/templates/footer');   
    }
    public function index()
    {
        $id = $this->session->userdata('id_user');

        $recevable = "recevable";

        $rejecter = "irrecevable";

        $simple = "simple";

        $complexe = "complexe";

        $homme = 'h';

        $femme = 'f';

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $nbrePlaintes = $this->President_model->countAllPlaint();
        $data['nbrePlaintes'] = $nbrePlaintes;

        $plaintesRecevable = $this->President_model->countPlaintesRecevable($recevable);
        $data['plaintesRecevable'] = $plaintesRecevable;

        $plaintesSimple = $this->President_model->countPlaintesSimple($simple);
        $data['plaintesSimple'] = $plaintesSimple;

        $plaintesComplexe = $this->President_model->countPlaintesComplexe($complexe);
        $data['plaintesComplexe'] = $plaintesComplexe;

        //Plainte par mois
        $plaintesMois = $this->President_model->countPlaintesParMois();
        $data['plaintesMois'] = $plaintesMois;

        //Type de compte et nbre d'utilisateur
        $nbretype = $this->President_model->typeUser();
        $data['nbretype'] = $nbretype;

        //Liste utilisateurs#
        $users = $this->President_model->ListUsers();
        $data['users'] = $users;

        //Plainte genre
        $plainteGenreH = $this->President_model->genreHomme($homme);
        $data['plainteGenreH'] = $plainteGenreH;

        $plainteGenreF = $this->President_model->genreFemme($femme);
        $data['plainteGenreF'] = $plainteGenreF;

        //Plainte rejeter
        $plaintesRejeter = $this->President_model->countPlaintesRejeter($rejecter);
        $data['plaintesRejeter'] = $plaintesRejeter;

        //Plainte enfant
        $plainteDate = $this->President_model->getAllDate();
        $data['plainteDate'] = $plainteDate;

        //Plainte par convention
        $plainteConvention = $this->President_model->plainteParConvention();
        $data['plainteConvention'] = $plainteConvention;


        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/accueilPresident', $data);
        $this->load->view('admin/templates/footer');
    }

    public function pdf($numeroDossier)
    {
        if($numeroDossier == "")
        {
            redirect('Admin/Error/');
        }
        //Path to download the pdf file
        $form = $this->Plainte_model->getDossierPlainte($numeroDossier);
        $data['form'] = $form;
        foreach($form as $d)
        {
            $num = $d->numero_dossier;
        }
        $this->load->view('admin/pages/report_page_view', $data, true);
        $trait = "_";
        $date =  preg_replace('/\s+/', '', date('d-m-Y'));
        $nom_user =  preg_replace('/\s+/', '', $num);
        $name_file = $nom_user.$trait.$date.'.pdf';
        //var_dump($name_file);die();
        $html = $this->load->view('admin/pages/report_page_view', $data, true);
        $mpdf = new \Mpdf\Mpdf(['format' => 'A4-P', 'tempDir' => './uploads/pdf/tmp']);
        $mpdf->SetHTMLFooter('<table width="100%" style="font-size: 8pt; color: #000000;">
        <tfoot>
        <tr>
            <td style="width: 70%;border: none;background-color: white;font-style: italic;">Ce document a été généré le {DATE j/m/Y H:i:s}</td>
            <td style="width: 20%;border: none;background-color: white;font-style: italic;" align="center">{PAGENO}/{nbpg}</td>
          </tr>
        </tfoot>

        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($name_file, 'I');
    }

    public function listePlainte(){
        $id = $this->session->userdata('id_user');

        $recevable = "recevable";

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $plainteValider = $this->President_model->getAllPlainte($recevable);
        $data['plainteValider'] = $plainteValider;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listePlaintePresident', $data);
        $this->load->view('admin/templates/footer');
    }

    public function presidentDecision($id_plainte)
    {
        $id = $this->session->userdata('id_user');

        $data['id_plainte'] = $id_plainte;

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/decisionPlaintesPresident_view', $data);
        $this->load->view('admin/templates/footer');
    }
    public function formDecision()
    {
        $this->form_validation->set_rules('decision', "nom", 'trim|required');
        $this->form_validation->set_rules('commentaire', 'commentaire', 'trim|required');

        $id_plainte = $this->input->post('id_plainte');

        $id = $this->session->userdata('id_user');

        if($this->form_validation->run() == true)
        {
            //true
            $decison = $this->input->post('decision');
            $commentaire = $this->input->post('commentaire');


            if($decison == "accepter")
            {
                $data = array(
                    'id_president' => $id,
                    'decision_president' => $decison,
                    'commentaire_president' => $commentaire,
                );

                $decision_requet = $this->President_model->decisionPresident($id_plainte, $data);

                if($decision_requet == TRUE){
                    $this->session->set_flashdata('error', 'Décision accepter.');
                    redirect('Admin/President/listePlainte');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/President/listePlainte');
                }
            }
            else{
                $data = array(
                    'id_president' => $id,
                    'decision_president' => $decison,
                    'commentaire_president' => $commentaire,
                );

                $decision_requet = $this->President_model->decisionPresident($id_plainte, $data);

                if($decision_requet == TRUE){
                    $this->session->set_flashdata('error', 'Décision refuser.');
                    redirect('Admin/President/listePlainte');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Admin/President/listePlainte');
                }
            }

        }
        else{
            //false
            $this->presidentDecision($id_plainte);
        }
    }
    public function plainteAccepter($id_plainte)
    {
        $id_president = $this->session->userdata('id_user');
        $decision = 'accepter';

        $data = array(
            'id_president' => $id_president,
            'decision_president' => $decision,
        );

        $decisionPresident = $this->President_model->decisionPresident($id_plainte, $data);

        if ($decisionPresident == TRUE){
            $action = "Plainte accepter";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Plainte accepter.');
            redirect('Admin/President/listePlainte');
        }
        else{
            $this->session->set_flashdata('success', 'Veuillez réessayer.');
            redirect('Admin/President/listePlainte');
        }
    }

    public function plainteRejeter($id_plainte)
    {
        $id_president = $this->session->userdata('id_user');
        $decision = 'rejeter';

        $data = array(
            'id_president' => $id_president,
            'decision_president' => $decision,
        );

        $decisionPresident = $this->President_model->decisionPresident($id_plainte, $data);

        if ($decisionPresident == TRUE){
            $action = "Plainte rejeter";
            $this->histoirque($action);
            $this->session->set_flashdata('success', 'Plainte rejeter.');
            redirect('Admin/President/listePlainte');
        }
        else{
            $this->session->set_flashdata('success', 'Veuillez réessayer.');
            redirect('Admin/President/listePlainte');
        }
    }

    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}