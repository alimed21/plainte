<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Formulaire de plainte</title>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/opensans-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <!-- Main Style Css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css"/>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <style>
        .genre{
            border: none !important;
        }
        .legend{
            font-size: 11px;
            font-weight: 700;
            color: #999;
        }
        .edit{
            margin: 0px;
            width: 454px;
            height: 247px;
        }
        .actions .clearfix{
            display: none;
        }
    </style>
</head>
<body>
<div class="page-content">
    <div class="form-v1-content">
        <div class="wizard-form">
            <form class="form-register" action="<?php echo base_url();?>index.php/Accueil/EnvoiePlainte" method="post" id="plainte"  enctype="multipart/form-data">
                <div id="form-total">
                    <!-- SECTION 1 -->
                    <h2>
                        <p class="step-icon"><span>01</span></p>
                        <span class="step-text">Information personnel</span>
                    </h2>
                    <section>
                        <div class="inner">
                            <div class="wizard-header">
                                <h3 class="heading">Infomation personnel</h3>
                                <p>Veuillez saisir vos informations.  </p>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Votre nom</legend>
                                        <input type="text" class="form-control" id="first-name" name="nom" placeholder="Votre nom" required>
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Profession</legend>
                                        <input type="text" class="form-control" id="profession" name="profession" placeholder="Veuillez saisir votre profession" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Genre</legend>
                                        <select name="genre" id="genre" class="form-control genre">
                                            <option value="h">Homme</option>
                                            <option value="f">Femme</option>
                                            <option value="e">Enfant</option>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Handicapé</legend>
                                        <select name="handicap" id="handicap" class="form-control genre">
                                            <option value="oui">Oui</option>
                                            <option value="non">Non</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Date de naissance</legend>
                                        <input type="date" class="form-control" id="date_naissance" name="date_naissance" placeholder="Veuillez saisir votre profession" required>
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Adresse</legend>
                                        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Veuillez saisir votre adresse" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Statut</legend>
                                        <select name="statut" id="statut" class="form-control genre">
                                            <option value="refugier">Réfugier</option>
                                            <option value="migrant">Migrant</option>
                                            <option value="demandeur asile">Demandeur d'asile</option>
                                            <option value="djiboutien(enne)">Djiboutien(enne)</option>
                                            <option value="autre">Autre</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Nationalité </legend>
                                        <input type="text" class="form-control" id="nationalite" name="nationalite" placeholder="Veuillez saisir votre nationalité" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Type document</legend>
                                        <select name="docu" id="docu" class="form-control genre">
                                            <option value="cni">CNI</option>
                                            <option value="passeport">Passeport</option>
                                            <option value="autre">Autre</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>N°Document</legend>
                                        <input type="text" class="form-control" id="numero" name="numero" placeholder="Veuillez saisir le numéro de votre document" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Email</legend>
                                        <input type="email" class="form-control" id="your_email" name="your_email" pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" placeholder="Votre mail...">
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Numéro</legend>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="77000000" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-check">
                                <legend class="legend">Cocher la bonne case</legend>
                                <input class="form-check-input" type="radio" name="statutPersonne" id="statut1" value="individuel" checked>
                                <label class="form-check-label" for="statut1">
                                    Individuel
                                </label>
                                <br>
                                <input class="form-check-input" type="radio" name="statutPersonne" id="statut2" value="conjoint">
                                <label class="form-check-label" for="statut2">
                                    Conjoint(e)
                                </label>
                                <br>
                                <input class="form-check-input" type="radio" name="statutPersonne" id="statut3" value="Représentant">
                                <label class="form-check-label" for="statut3">
                                    Représentant
                                </label>
                            </div>
                        </div>
                    </section>
                    <!-- SECTION 2 -->
                    <h2>
                        <p class="step-icon"><span>02</span></p>
                        <span class="step-text">Renseignement plainte</span>
                    </h2>
                    <section>
                        <div class="inner">
                            <div class="wizard-header">
                                <h3 class="heading">Renseignement de la plainte</h3>
                                <p>Veuillez saisir toutes les champs.  </p>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>Personne ou Organisation</legend>
                                        <input type="text" class="form-control" id="nom_plainte" name="nom_plainte" placeholder="Personne ou Organisation contre lequel la plainte est déposée" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Adresse</legend>
                                        <input type="text" class="form-control" id="adresse_plainte" name="adresse_plainte" placeholder="Veuillez saisir l'adresse" required>
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>Téléphone</legend>
                                        <input type="text" class="form-control" id="tel_plainte" name="tel_plainte" placeholder="Veuillez saisir le numéro téléphone" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>Nature de la plainte</legend>
                                        <input type="text" class="form-control" id="nature_plainte" name="nature_plainte" placeholder="Veuillez saisir la nature de la plainte" required>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>Langue de la plainte</legend>
                                        <input type="text" class="form-control" id="langue_plainte" name="langue_plainte" placeholder="Veuillez saisir la langue de la plainte" required>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- SECTION 3 -->
                    <h2>
                        <p class="step-icon"><span>03</span></p>
                        <span class="step-text">Motifs plainte</span>
                    </h2>
                    <section>
                        <div class="inner">
                            <div class="wizard-header">
                                <h3 class="heading">Motifs de la plainte</h3>
                                <p>Veuillez saisir toutes les champs.</p>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Déscription de la plainte</label>
                                    <textarea class="form-control edit" id="editor1" name="plainte" cols="30" rows="10" style="visibility: none"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Pièces joints</label>
                                <input type="file" name="file_upload[]" id="file"  multiple="multiple">
                                <p>Nb* : les pièces joints doivent être de type : .pdf, .png, .jpg, .jpeg et .mp3</p>
                            </div>

                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                  <button type="submit" id="btn">Envoyer</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.steps.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets/admin/ckeditor/ckeditor.js'); ?>"></script>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    $("#editor1").css("visibility", "visible");
</script>
<script>
	$(function(){
            $("#btn").click(function(){
               var $fileUpload = $("input[type='file']");
               if (parseInt($fileUpload.get(0).files.length) > 10){
                  alert("Vous etes autorisé à selectionner que dix fichiers");
               }
            });
         });

</script>

</body>
</html>