<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des types comptes</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Libellé </th>
                                        <th>Ajouté(e) par</th>
                                        <th>Ajouté(e) le</th>
                                        <th>Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($typeComptes as $type):?>
                                        <tr>
                                            <td><?php echo $type->nom_type;?></td>
                                            <td><?php echo $type->login_admin;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($type->date_add_type));?></td>
                                            <td><a href="<?php echo base_url();?>Admin/Administrateur/SupprimerTypeCompte/<?php echo $type->id_type;?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>