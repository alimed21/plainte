<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Modification du mot de passe</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if (isset($error_message)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error_message; ?>
                            </div>
                        <?php } ?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Admin/ParametresAdmin/modificationMotPasse" method="post">

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <input type="password" id="ancienpass" name="ancienpass" placeholder="Saissiez votre ancien mot de passe..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('ancienpass'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <input type="password" id="nouveaupass" name="nouveaupass" placeholder="Saissiez votre nouveau mot de passe..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('nouveaupass'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <input type="password" id="cnfpass" name="cnfpass" placeholder="Confirmation du nouveau mot de passe..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('cnfpass'); ?></span>
                                </div>

                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-primary ">Modifier</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>