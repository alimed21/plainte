<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Ajout d'un utilisateur</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                            <div class="card-body card-block">
                                <form action="<?php echo base_url();?>Admin/Administrateur/VerifyForm" method="post" class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="username" name="username" placeholder="Nom d'utilisateur..." class="form-control">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('username'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="password" id="password" name="password" placeholder="Mot de passe..." class="form-control">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('password'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="password" id="cnfpassword" name="cnfpassword" placeholder="Confirmation du mot de passe..." class="form-control">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('cnfpassword'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="email" id="email" name="email" placeholder="Email..." class="form-control">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('email'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select name="type" id="type" class="form-control">
                                                <option value="" disabled selected>Choisissez un type de compte</option>
                                                <?php foreach ($typeComptes as $type):?>
                                                    <option value="<?php echo $type->id_type;?>"><?php echo $type->nom_type;?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('type'); ?></span>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>