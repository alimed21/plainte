    <!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Gestion des plaintes</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url();?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url();?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url();?>assets/admin/css/theme.css" rel="stylesheet" media="all">

    <!-- CSS rules for styling the element inside the editor such as p, h1, h2, etc. -->
    <link href="<?php echo base_url();?>assets/froala/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!--wysiwgg editor-->
    <link href="<?php echo base_url();?>assets/froala/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/froala/js/froala_editor.pkgd.min.js"></script>

    <!--eChart-->
    <script src="<?php echo base_url();?>assets/echarts/dist/echarts.min.js"></script>

    <!-- Style CSS-->
    <style>
        .infoMessage{
            color:red;
            font-size: 15px;
        }
        .table-top-campaign.table tr td:last-child {
            text-align: left !important;
        }
    </style>

</head>

<body class="animsition">
<div class="page-wrapper">
    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar2">
        <div class="logo">
            <a href="#">
                <h3 style="color: white;font-size: 22px">Gestion des plaintes</h3>
            </a>
        </div>
        <div class="menu-sidebar2__content js-scrollbar1">
            <div class="account2">
                <?php foreach ($infoUser as $user):?>
                    <div class="image img-cir img-120">
                        <img src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Photo" />
                    </div>
                    <h4 class="name"><?php echo $user->username;?></h4>
                    <a href="<?php echo base_url();?>Admin/Login/logout">Déconnexion</a>
                <?php endforeach;?>
            </div>
            <nav class="navbar-sidebar2">
                <ul class="list-unstyled navbar__list">
                    <?php if ($this->session->userdata('nom_type') == "Secretariat" ):?>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretariat">
                                <i class="fas fa-tachometer-alt"></i>Liste des plaintes
                            </a>
                        </li>
                       <!-- -->
                    <?php elseif($this->session->userdata('nom_type') == "Agent"):?>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent">
                                <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                            </a>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent/affectPlaint">
                                <i class="fas fa-tachometer-alt"></i>Affecter les plaintes
                            </a>
                        </li>
                    <?php elseif($this->session->userdata('nom_type') == "Secrétaire Général" ):?>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General">
                                <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                            </a>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General/Affecter">
                                <i class="fas fa-tachometer-alt"></i>Affectation des plaintes
                            </a>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General/categorie">
                                <i class="fas fa-tachometer-alt"></i>Catégorie plainte
                            </a>
                        </li>
                    <?php else:?>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/President">
                                <i class="fas fa-tachometer-alt"></i>Accueil
                            </a>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Admin/President/listePlainte">
                                <i class="fas fa-tachometer-alt"></i>Voir plaintes
                            </a>
                        </li>
                    <?php endif;?>

                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container2">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop2">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap2">
                        <div class="logo d-block d-lg-none">
                            <a href="#">
                                <h3 style="color: white;font-size: 18px">Gestion des plaintes</h3>
                            </a>
                        </div>
                        <div class="header-button2">
                            
                            <div class="header-button-item mr-0 js-sidebar-btn">
                                <i class="zmdi zmdi-menu"></i>
                            </div>
                            <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Admin/Profile">
                                            <i class="zmdi zmdi-account"></i>Profile</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Admin/Parametres">
                                            <i class="zmdi zmdi-settings"></i>Parametres</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Admin/Login/logout">
                                            <i class="zmdi zmdi-sign-in"></i>Déconnexion</a>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
            <div class="logo">
                <a href="#">
                    <img src="<?php echo base_url();?>assets/admin/images/icon/logo-white.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar2">
                <div class="account2">
                    <?php foreach ($infoUser as $user):?>
                    <div class="image img-cir img-120">
                        <img src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Photo" />
                    </div>
                    <h4 class="name"><?php echo $user->username;?></h4>
                    <?php endforeach;?>
                    <a href="<?php echo base_url();?>Admin/Login/logout">Déconnexion</a>
                </div>
                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">
                        <?php if ($this->session->userdata('nom_type') == "Secretariat" ):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretariat">
                                    <i class="fas fa-tachometer-alt"></i>Liste des plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretariat/Affecter">
                                    <i class="fas fa-tachometer-alt"></i>Affectation des plaintes
                                </a>
                            </li>
                        <?php elseif($this->session->userdata('nom_type') == "Agent"):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent">
                                    <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent/Affecter">
                                    <i class="fas fa-tachometer-alt"></i>Affecter les plaintes
                                </a>
                            </li>
                        <?php elseif($this->session->userdata('nom_type') == "Secrétaire Général" ):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General">
                                    <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General/categorie">
                                    <i class="fas fa-tachometer-alt"></i>Catégorie plainte
                                </a>
                            </li>
                        <?php else:?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/President">
                                    <i class="fas fa-tachometer-alt"></i>Accueil
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/President/listePlainte">
                                    <i class="fas fa-tachometer-alt"></i>Voir plaintes
                                </a>
                            </li>
                        <?php endif;?>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END HEADER DESKTOP-->
