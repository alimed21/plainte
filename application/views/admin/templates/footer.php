<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p>Copyright © 2020 CNDH. Développer par <a href="#">CCEI</a>.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END PAGE CONTAINER-->
</div>

</div>

<!-- Jquery JS-->
<script src="<?php echo base_url();?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="<?php echo base_url();?>assets/admin/vendor/slick/slick.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/wow/wow.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/animsition/animsition.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/select2/select2.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.sampledata.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.world.js"></script>

<!-- Main JS-->
<script src="<?php echo base_url();?>assets/admin/js/main.js"></script>

<script>
    $( "#irrecev" ).hide();
    $( "#recev" ).hide();

    $(document).ready(function(){
    $('#decision').on('change', function() {
      if ( this.value == 'recevable')
      {
        $( "#irrecev" ).hide();
        $("#recev").show();
      }
      else
      {
        $( "#recev" ).hide();
        $("#irrecev").show();
      }
    });
});

</script>

<script>
    var editor = new FroalaEditor('#example')
</script>
</body>

</html>
<!-- end document-->