<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Modification du profile</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if (isset($error_message)):?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error_message; ?>
                            </div>
                        <?php endif; ?>
                        <div class="card-body card-block">
                            <?php echo form_open_multipart('Admin/Profile/VerifyForm');?>
                                 <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" id="nom" name="nom" placeholder="Nom complet..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('nom'); ?></span>
                                 </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="number" id="telephone" name="telephone" placeholder="Votre numéro téléphone..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('telephone'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-address-card"></i>
                                        </div>
                                        <input type="text" id="adresse" name="adresse" placeholder="Votre adresse..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('adresse'); ?></span>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <input type="text" id="localisation" name="localisation" placeholder="Saissiez votre localisation..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('localisation'); ?></span>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-picture-o"></i>
                                        </div>
                                        <input type="file" id="userfile" name="userfile" class="form-control">
                                    </div>
                                </div>

                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>