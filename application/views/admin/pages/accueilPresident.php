<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BREADCRUMB-->
<!-- STATISTIC-->
<section class="statistic statistic2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--green">
                    <?php foreach ($nbrePlaintes as $total):?>
                    <h2 class="number"><?php echo $total->plaintes;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Total plaintes</span>
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--orange">
                    <?php foreach ($plaintesRecevable as $recevable):?>
                        <h2 class="number"><?php echo $recevable->recevable;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plainte recevalble</span>
                    <div class="icon">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--blue">
                    <?php foreach ($plaintesSimple as $simple):?>
                        <h2 class="number"><?php echo $simple->simple;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plaintes simples</span>
                    <div class="icon">
                        <i class="zmdi zmdi-calendar-note"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--red">
                    <?php foreach ($plaintesComplexe as $complexe):?>
                        <h2 class="number"><?php echo $complexe->complexe;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plaintes complexes</span>
                    <div class="icon">
                        <i class="zmdi zmdi-money"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC-->
<!-- STATISTIC CHART-->
<section class="statistic-chart">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Statistiques</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <!-- TOP CAMPAIGN-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">Type de compte</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                                <?php foreach ($nbretype as $type):?>
                                    <tr>
                                        <td><?php echo $type->nom_type?></td>
                                        <td><?php echo $type->nbr;?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END TOP CAMPAIGN-->
            </div>
            <div class="col-md-6 col-lg-4">
                <!-- CHART PERCENT-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">Plaintes genre</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                                <?php foreach ($plainteGenreH as $h):?>
                                    <tr>
                                        <td>Homme</td>
                                        <td><?php echo $h->sexe;?></td>
                                    </tr>
                                <?php endforeach;?>
                                <?php foreach ($plainteGenreF as $f):?>
                                    <tr>
                                        <td>Femme</td>
                                        <td><?php echo $f->sexe;?></td>
                                    </tr>
                                <?php endforeach;?>
                                <?php foreach ($plainteDate as $enfant):?>
                                    <tr>
                                        <td>Enfant</td>
                                        <td><?php echo $enfant->under_18;?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CHART PERCENT-->
            </div>
            <div class="col-md-6 col-lg-4">
                <!-- CHART PERCENT-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">Type de plainte</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                            <?php foreach ($nbrePlaintes as $nbre):?>
                                <tr>
                                    <td>Total plainte</td>
                                    <td><?php echo $nbre->plaintes;?></td>
                                </tr>
                            <?php endforeach;?>
                            <?php foreach ($plainteGenreH as $h):?>
                                <tr>
                                    <td>Homme</td>
                                    <td><?php echo $h->sexe;?></td>
                                </tr>
                            <?php endforeach;?>
                            <?php foreach ($plainteGenreF as $f):?>
                                <tr>
                                    <td>Femme</td>
                                    <td><?php echo $f->sexe;?></td>
                                </tr>
                            <?php endforeach;?>
                            <?php foreach ($plaintesRejeter as $rejeter):?>
                                <tr>
                                    <td>Plaintes rejecter</td>
                                    <td><?php echo $rejeter->irrecevable;?></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CHART PERCENT-->
            </div>

            <div class="col-md-4">
                <!-- CHART PERCENT-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">Plainte par convention</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <thead>
                                <th>Convention</th>
                                <th>Chiffres</th>
                            </thead>
                            <tbody>
                                <?php foreach ($plainteConvention as $convention):?>
                                    <tr>
                                        <td>
                                            <?php echo $convention->nom_court;?>
                                        </td>
                                        <td>
                                            <?php echo $convention->convention;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CHART PERCENT-->
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC CHART-->
