<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Profile</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title mb-3">Profile</strong>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach($profileUser as $user):?>
                                            <div class="mx-auto d-block">
                                                <img class="rounded-circle mx-auto d-block" src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Card image cap">
                                                <h5 class="text-sm-center mt-2 mb-1"><?php echo $user->nom_user;?></h5>
                                                <div class="location text-sm-center">
                                                    <i class="fa fa-map-marker"></i> <?php echo $user->adresse_user;?>, <?php echo $user->loaclisation_user;?></div>
                                                <div class="location text-sm-center">
                                                    <i class="fa fa-phone"></i> <?php echo $user->telephone_user;?></div>
                                                <div class="location text-sm-center">
                                                    <i class="fa fa-envelope"></i> <?php echo $user->email;?></div>

                                            </div>
                                            <hr>
                                            <div class="card-text text-sm-center">
                                                <a href="<?php echo base_url();?>Admin/Profile/modifierProfile">
                                                    <button class="btn btn-primary center">Modifier</button>
                                                </a>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>