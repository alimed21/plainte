<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Ajout d'un annuaire</div>

                        <?php if (validation_errors() != false) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php } ?>
                        <?php if (isset($error_message)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error_message; ?>
                            </div>
                        <?php } ?>

                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Admin/Accueil/VerifyForm" method="post" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input type="text" id="nom" name="nom" placeholder="Nom complet..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('nom'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select name="depart" id="depart" class="form-control">
                                            <option value="" disabled selected>Choisissez un département</option>
                                            <?php foreach ($departement as $depart):?>
                                                <option value="<?php echo $depart->id_depart;?>"><?php echo $depart->depart;?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select name="fonction" id="fonction" class="form-control">
                                            <option value="" disabled selected>Choisissez une fonction</option>
                                            <?php foreach ($depart_fonction as $fonction):?>
                                                <option value="<?php echo $fonction->id_fonction;?>"><?php echo $fonction->nom_fonction;?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="number" id="fixe" name="fixe" placeholder="Numéro téléphone..." class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="number" id="interne" name="interne" placeholder="Numéro interne..." class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <input type="email" id="email" name="email" placeholder="Email..." class="form-control">
                                    </div>
                                </div>

                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>