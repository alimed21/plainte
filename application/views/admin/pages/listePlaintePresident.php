<!-- MAIN CONTENT-->
<style>
    .btnPdf{
        color:white;
    }
    .btnPdf:hover{
        color: white;
    }
</style>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'success' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('success'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>N°dossier</th>
                                        <th>Décision SG</th>
                                        <th>Type de plainte</th>
                                        <th>Voir</th>
                                        <th>Décision Président</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($plainteValider as $plainte):?>
                                        <tr>
                                            <td><?php echo $plainte->numero_dossier;?></td>
                                            <td><?php echo $plainte->decision;?></td>
                                            <td><?php echo $plainte->type_plainte;?></td>
                                            <!--<td><?php /*echo date("d-m-Y", strtotime($plainte->date_affect ));*/?></td>-->
                                            <td>
                                                <button class="btn btn-success">
                                                    <a href="<?php echo base_url();?>Admin/President/pdf/<?php echo $plainte->id_plainte; ?>" class="btnPdf" target="_blank">PDF</a>
                                                </button>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url();?>Admin/President/presidentDecision/<?php echo $plainte->id_plainte;?>" class="btnPdf"><button class="btn btn-primary">Cliquer ici</button></a>
                                                <a href="<?php echo base_url();?>Admin/President/plainteAccepter/<?php echo $plainte->id_plainte;?>" ></a>
                                                <a href="<?php echo base_url();?>Admin/President/plainteRejeter/<?php echo $plainte->id_plainte;?>" class="btnPdf"></a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>