<!-- MAIN CONTENT-->
<style>
    .btnPdf{
        color:white;
    }
    .btnPdf:hover{
        color: white;
    }
</style>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>N°dossier</th>
                                        <th>Nom</th>
                                        <th>Date de naissance</th>
                                        <th>Nationalité</th>
                                        <th>Adresse</th>
                                        <th>Téléphone ou mail</th>
                                        <th>Générer en pdf</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($plaintesAffecter as $plainte):?>
                                        <tr>
                                            <td><?php echo $plainte->numero_dossier;?></td>
                                            <td><?php echo $plainte->nom_prenom;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($plainte->date_naissance));?></td>
                                            <td><?php echo $plainte->nationalite;?></td>
                                            <td><?php echo $plainte->adresse;?></td>
                                            <td><?php echo $plainte->telephon;?><br><?php echo $plainte->email;?></td>
                                            <td>
                                                <button class="btn btn-success">
                                                    <a href="<?php echo base_url();?>Admin/Agent/pdf/<?php echo $plainte->id_plainte; ?>" class="btnPdf" target="_blank">PDF</a>
                                                </button>
                                            </td>

                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>