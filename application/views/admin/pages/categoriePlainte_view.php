<!-- MAIN CONTENT-->
<style>
    .btnPdf{
        color:white;
    }
    .btnPdf:hover{
        color: white;
    }
</style>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'success' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('success'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>N°plainte</th>
                                        <th>N°dossier</th>
                                        <th>Rapport</th>
                                        <th>Simple</th>
                                        <th>Complexe</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($plaintesSG_valid as $plainte):?>
                                        <tr>
                                            <td><?php echo $plainte->id_plainte;?></td>
                                            <td><?php echo $plainte->numero_dossier;?></td>
                                            <td>
                                                <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#scrollmodal<?php echo $plainte->id_plainte;?>">
                                                    Voir le rapport
                                                </button>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url();?>Admin/Secretaire_General/plainteSimple/<?php echo $plainte->id_plainte;?>" class="btnPdf"><button class="btn btn-success">Plainte simple</button></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url();?>Admin/Secretaire_General/plainteComplexe/<?php echo $plainte->id_plainte;?>" class="btnPdf"><button class="btn btn-primary">Plainte complexe</button></a>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal scroll -->
<?php foreach ($plaintesSG_valid as $plainte):?>
    <div class="modal fade" id="scrollmodal<?php echo $plainte->id_plainte;?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Rapport d’évaluation initial:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="color: black;">
                    <?php echo $plainte->commentaire;?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>

<!-- end modal scroll -->