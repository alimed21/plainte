<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BREADCRUMB-->
<!-- STATISTIC-->
<section class="statistic statistic2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--green">
                    <?php foreach ($nbrePlaintes as $total):?>
                    <h2 class="number"><?php echo $total->plaintes;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Total plaintes</span>
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--orange">
                    <?php foreach ($plaintesRecevable as $recevable):?>
                        <h2 class="number"><?php echo $recevable->recevable;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plainte recevalble</span>
                    <div class="icon">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--blue">
                    <?php foreach ($plaintesSimple as $simple):?>
                        <h2 class="number"><?php echo $simple->simple;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plaintes simples</span>
                    <div class="icon">
                        <i class="zmdi zmdi-calendar-note"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="statistic__item statistic__item--red">
                    <?php foreach ($plaintesComplexe as $complexe):?>
                        <h2 class="number"><?php echo $complexe->complexe;?></h2>
                    <?php endforeach;?>
                    <span class="desc">Plaintes complexes</span>
                    <div class="icon">
                        <i class="zmdi zmdi-money"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC-->
<!-- STATISTIC CHART-->
<section class="statistic-chart">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Statistiques</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <!-- CHART-->
                <h3 class="title-3 m-b-30">Plainte par genre</h3>
                <div id="main" style="width: 600px;height:400px;margin-top: 75px;"></div>

                <script type="text/javascript">
                    // based on prepared DOM, initialize echarts instance
                    var myChart = echarts.init(document.getElementById('main'));

                    // specify chart configuration item and data
                    option = {
                        tooltip: {
                            trigger: 'item',
                            formatter: '{a} <br/>{b}: {c} ({d}%)'
                        },
                        legend: {
                            orient: 'vertical',
                            left: 10,
                            data: ['Homme', 'Femme', 'Enfant']
                        },
                        series: [
                            {
                                name: 'Plainte par genre',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                avoidLabelOverlap: false,
                                label: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    label: {
                                        show: true,
                                        fontSize: '30',
                                        fontWeight: 'bold'
                                    }
                                },
                                labelLine: {
                                    show: false
                                },
                                data: [
                                    {value: <?php echo $plainteGenreH->sexe;?>, name: 'Homme'},
                                    {value: <?php echo $plainteGenreF->sexe;?>, name: 'Femme'},
                                    {value: <?php echo $plainteGenreE->sexe;?>, name: 'Enfant'}
                                ]
                            }
                        ]
                    };
                    // use configuration item and data specified to show chart
                    myChart.setOption(option);
                </script>
                <!-- END CHART-->
            </div>
            <div class="col-md-6 col-lg-4">
                <h3 class="title-3 m-b-30">Plainte handicap</h3>
                <div id="main2" style="width: 600px;height:400px;margin-top: 75px;"></div>

                <script type="text/javascript">
                    // based on prepared DOM, initialize echarts instance
                    var myChart = echarts.init(document.getElementById('main2'));

                    // specify chart configuration item and data
                    option = {
                        tooltip: {
                            trigger: 'item',
                            formatter: '{a} <br/>{b}: {c} ({d}%)'
                        },
                        legend: {
                            orient: 'vertical',
                            left: 10,
                            data: ['Handicapé', 'Non-handicapé']
                        },
                        series: [
                            {
                                name: 'Plainte handicap',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                avoidLabelOverlap: false,
                                label: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    label: {
                                        show: true,
                                        fontSize: '30',
                                        fontWeight: 'bold'
                                    }
                                },
                                labelLine: {
                                    show: false
                                },
                                data: [
                                    {value: <?php echo $plainteHandicap->handicap;?>, name: 'Handicapé'},
                                    {value: <?php echo $plainteNonHandicap->nonhandicap;?>, name: 'Non-handicapé'}
                                ]
                            }
                        ]
                    };
                    // use configuration item and data specified to show chart
                    myChart.setOption(option);
                </script>
                <!-- END CHART-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-3 m-b-30">Nombre des plaintes par mois</h3>
                <div id="plaitByMonths" style="width: 600px;height:400px;margin-top: 75px;"></div>

            <script type="text/javascript">
                // based on prepared DOM, initialize echarts instance
                var myChart = echarts.init(document.getElementById('plaitByMonths'));
                let plaintesMois = <?php echo json_encode($plaintesMois);?>;
                let month = [];
                let count = [];
                
                

                for(var i = plaintesMois.length - 1; i >= 0; i--){
                    month.push((plaintesMois[i]).month);
                    count.push((plaintesMois[i]).count);
                }

                // specify chart configuration item and data
                option = {
                color: ['#3398DB'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: month,
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: 'Nombre de plainte',
                        type: 'bar',
                        barWidth: '60%',
                        data: count
                    }
                ]
                };

                // use configuration item and data specified to show chart
                myChart.setOption(option);
            </script>
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC CHART-->
