<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if ( $this->session->flashdata( 'success' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('success'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Admin/Agent/Affectation" method="post" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-folder"></i>
                                        </div>
                                        <select name="plainte" id="plainte" class="form-control">
                                            <option selected="true" disabled="disabled">Choisissez une plainte</option>
                                            <?php foreach($plaintesNumero as $num):?>
                                                <option value=" <?php echo $num->id_plainte;?>"><?php echo $num->numero_dossier;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('plainte'); ?></span>
                                </div>
                              <div class="form-group">
                                    <!--Material textarea-->
                                    <div class="md-form">
                                        <textarea id="example" class="md-textarea form-control" name="commentaire" rows="3" placeholder="Ajouter votre commentaire..."></textarea>
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('commentaire'); ?></span>
                                </div>
                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>