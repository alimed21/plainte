<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

        body {

            font-family: "Roboto", helvetica, arial, sans-serif;
            font-size: 16px;
            font-weight: 400;
            text-rendering: optimizeLegibility;
        }

        div.table-title {
            display: block;
            margin: auto;
            max-width: 600px;
            padding:5px;
            width: 100%;
        }

        .table-title h3 {
            color: #fafafa;
            font-size: 30px;
            font-weight: 400;
            font-style:normal;
            font-family: "Roboto", helvetica, arial, sans-serif;
            text-transform:uppercase;
        }


        /*** Table Styles **/

        .table-fill, .table-fill2, .table-fill3  {
            background: white;
            border-radius:3px;
            border-collapse: collapse;
            margin: auto;
            padding:5px;
            width: 100%;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
            animation: float 5s infinite;
        }


        th {
            color:white;;
            background:#1b1e24;
            border-bottom:4px solid #9ea7af;
            border-right: 1px solid #343a45;
            font-size:23px;
            font-weight: 100;
            padding:5px;
            text-align:left;
            vertical-align:middle;
        }

        th:first-child {
            border-top-left-radius:3px;
        }

        th:last-child {
            border-top-right-radius:3px;
            border-right:none;
        }

        tr {
            border-top: 1px solid #C1C3D1;
            border-bottom: 1px solid #C1C3D1;
            color:#666B85;
            font-size:16px;
            font-weight:normal;
        }

        tr:hover td {
            background:#4E5066;
            color:#FFFFFF;
            border-top: 1px solid #22262e;
        }

        tr:first-child {
            border-top:none;
        }

        tr:last-child {
            border-bottom:none;
        }

        tr:nth-child(odd) td {
            background:#EBEBEB;
        }

        tr:nth-child(odd):hover td {
            background:#4E5066;
        }

        tr:last-child td:first-child {
            border-bottom-left-radius:3px;
        }

        tr:last-child td:last-child {
            border-bottom-right-radius:3px;
        }

        td {
            background:#FFFFFF;
            padding:5px;
            text-align:left;
            vertical-align:middle;
            font-weight:300;
            font-size:15px;
            border-right: 1px solid #C1C3D1;
            height: 30px;
        }

        td:last-child {
            border-right: 0px;
        }

        th.text-left {
            text-align: left;
        }

        th.text-center {
            text-align: center;
        }

        th.text-right {
            text-align: right;
        }

        td.text-left {
            text-align: left;
        }

        td.text-center {
            text-align: center;
        }

        td.text-right {
            text-align: right;
        }
        caption {
            background: #6AB2E7;
            padding: 10px;
            font-size:20px;
            color: white;
            text-align: left;
        }
        td{
            color:black;
        }
        .image{
            text-align:right;
        }
        .title_tr td{
            font-size: 20px;
            color: black;
            background-color: #1b1e24 !important;
            width: 45%;
            height: 6%;
        }
        .corps{
            margin-top: -20px;
        }
    </style>
</head>
<body>
<div style="text-align:center"><img src="<?php echo base_url();?>assets/images/maquette/top-bar2.png" alt="Logo" style="width: 100%;"></div>
<h1 style="text-align:center">Demande de plainte</h1>
<div class="corps">
    <?php foreach($form as $row):?>
        <p style="color:black;font-size:14px;text-align:center;font-weight:bold;margin-top: -20px;">Enregistrée, le <?php echo date('d-m-Y', strtotime($row->date_ajout_plainte)); ?></p>
    <?php endforeach;?>

    <table class="table-fill" style="margin-top: 10px;">
        <?php foreach($form as $row):?>
            <caption>Renseignements Personnels</caption>
            <tr>
                <td style="width: 332px;">Nom </td>
                <td><?php echo $row->nom_prenom; ?></td>
            </tr>
            <tr>
                <td>Date et lieu de naissance </td>
                <td>le <?php echo date('d-m-Y', strtotime($row->date_naissance)); ?></td>
            </tr>
            <tr>
                <td>Nationalité</td>
                <td><?php echo $row->nationalite; ?></td>
            </tr>
            <tr>
                <td>Sexe</td>
                <td>
                    <?php if($row->sexe == 'h'){
                        echo "Homme";
                    }else{
                        echo "Femme";
                    };?>
                </td>
            </tr>
            <tr>
                <td>Adresse</td>
                <td><?php echo $row->adresse; ?></td>
            </tr>
            <tr>
                <td>Profession</td>
                <td><?php echo $row->profession; ?></td>
            </tr>
            <tr>
                <td>Coordonnées</td>
                <td>Tél : <?php echo $row->telephon; ?>, Email : <?php echo $row->email; ?></td>
            </tr>
            <tr>
                <td>Statut de la personne </td>
                <td><?php echo $row->statut_personne; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <table class="table-fill">
        <?php foreach($form as $row):?>
            <caption>Renseignement plainte</caption>
            <tr>
                <td style="width: 332px;">Nom de l'organisation ou de la personne contre lequel la plainte est déposéé"</td>
                <td><?php echo $row->nom_plainte; ?></td>
            </tr>
            <tr>
                <td>Adresse complet  </td>
                <td><?php echo $row->adresse_plainte; ?></td>
            </tr>
            <tr>
                <td>Téléphone  </td>
                <td><?php echo $row->tel_plainte; ?></td>
            </tr>
            <tr>
                <td>Nature de la plainte </td>
                <td><?php echo $row->nature_plainte; ?></td>
            </tr>
            <tr>
                <td>Langue de la plainte </td>
                <td><?php echo $row->langue_plainte; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <br>
    <table class="table-fill">
        <caption>Objet, dates et lieux de la mission</caption>
        <?php foreach($form as $row):?>
            <tr>
                <td style="width: 332px;">Déscription de la plainte  </td>
                <td><?php echo $row->description_plainte; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
</body>
</html>