<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Décision président</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if ( $this->session->flashdata( 'success' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('success'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Admin/President/formDecision" method="post" class="">
                                <input type="hidden" name="id_plainte" value="<?php echo $id_plainte; ?>">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select name="decision" id="decision" class="form-control">
                                            <option selected="true" disabled="disabled">Décision </option>
                                            <option value="accepter">Accepter </option>
                                            <option value="refuser">Refuser </option>
                                        </select>
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('decision'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea name="commentaire" id="editor1" cols="30" rows="10"></textarea>
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('commentaire'); ?></span>
                                </div>
                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/admin/ckeditor/ckeditor.js'); ?>"></script>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
</script>
