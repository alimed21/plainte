<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3">Mot de passe</strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <img class="rounded-circle mx-auto d-block" src="<?php echo base_url();?>assets/admin/images/pass.jpg" alt="Card image cap">
                                <h5 class="text-sm-center mt-2 mb-1">Modifier votre ancien mot de passe</h5>
                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a href="<?php echo base_url();?>Admin/Parametres/motPasse">
                                    <button class="btn btn-primary center">Modifier</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3">Photo de profile</strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <?php foreach($profileUser as $user):?>
                                    <img class="rounded-circle mx-auto d-block" src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Card image cap" style="height: 170px">
                                <?php endforeach;?>
                                <h5 class="text-sm-center mt-2 mb-1">Modifier votre photo de profile</h5>
                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a href="<?php echo base_url();?>Admin/Profile/modifierPhotoProfile">
                                    <button class="btn btn-primary center">Modifier</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3">Information profile</strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <img class="rounded-circle mx-auto d-block" src="<?php echo base_url();?>assets/admin/images/user.jpg" alt="Card image cap" style="height: 170px;">
                                <h5 class="text-sm-center mt-2 mb-1">Modifier votre profile</h5>
                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a href="<?php echo base_url();?>Admin/Profile/modifierProfile">
                                    <button class="btn btn-primary center">Modifier</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>